#!/bin/sh

<<COMMENT

Certify/refresh_keys.sh - Generate and auto-update elliptic and RSA keys.

Copyright (c) 2020 Nathan Harmon

COMMENT

# Service to restart
service="nginx"

# The location of this file
loc="$(dirname $0)"

# The beginning of the elliptic and RSA private key file name. Don't put a number here.
eheader="ellippriv"
rheader="rsapriv"

# The beginning of the elliptic and RSA cert file name. Don't put a number here.
ecert="ellicert"
rcert="rsacert"

# The file path of the Let's Encrypt account private key.
account="$loc/account.key"

# The directory where acme challenges are stored.
challenge="/var/www/challenges/"

# The type of key to be generated. Options: secp256r1 ; secp384r1
keytype="secp384r1"

# The size of the RSA key to generate.
size="4096"

# How many keys should be kept in history before deleting. Set to zero to not keep any history. (Default: 3)
history=3

# Helper function to exit the program with a failure message.
# $1: Exit Code ; $2: Message to output to the user.
fail() {
	echo $2
	exit $1
}

id certify >/dev/null 2>&1 || fail 1 "Please create the certify user with # useradd certify --system --no-create-home"

# Generate files and set their permissions and owner.
chown certify:certify $loc
mkdir -m 700 $loc/certs $loc/keys 2>/dev/null && chown certify:certify $loc/certs $loc/keys
ls $loc/openssl.cnf >/dev/null 2>&1 || sudo -u certify wget https://gitlab.com/nathanharmon/certify/-/raw/master/openssl.cnf -P $loc/ 2>/dev/null && chmod 600 $loc/openssl.cnf

# Various checks to make sure the script will work before it gets started.
[ $(ls -la $challenge | sed '2!d'  | cut -d" " -f3) = "certify" ] || fail 1 "Please make sure $challenge is owned by the certify user."
ls $loc/acme_tiny.py >/dev/null 2>&1 || fail 1 "Please download acme_tiny.py from https://github.com/diafygi/acme-tiny"
ls $loc/account.key >/dev/null 2>&1 || fail 1 "Please get your account.key file."
grep example.com $loc/openssl.cnf >/dev/null && fail 1 "Please modify openssl.cnf for your domain name."

# Increment existing keys or certs (in the reverse order to prevent naming conflictions)
# $1: folder to increment
increment_folder() {
	for file in $(ls -1 $loc/$1 | grep -E "[0-9]\.(pem|crt)$" | sort -r)
	do
		prefix=$(echo $file | cut -d. -f1 | tr -d [0-9])
		id=$(echo $file | grep -o -E [0-9])
		type=$(echo $file | cut -d. -f2)

		# Delete the oldest keys and certs
		[ $id -gt $history ] && rm $loc/$1/$file && continue

		mv $loc/$1/$prefix$id.$type $loc/$1/$prefix$((id+1)).$type
	done
}

[ $history -ne 0 ] && increment_folder keys && increment_folder certs

# Generate elliptic key and have it signed by CA
openssl ecparam -genkey -name $keytype -out $loc/keys/$eheader\0.pem
openssl req -new -config $loc/openssl.cnf -key $loc/keys/$eheader\0.pem -out $loc/csr.csr

sudo -u certify python $loc/acme_tiny.py --account-key $account --csr $loc/csr.csr --acme-dir $challenge > $loc/certs/$ecert\0.crt

# Generate RSA key and have it signed by CA
openssl genrsa -out $loc/keys/$rheader\0.pem $size 2>/dev/null
openssl req -new -config $loc/openssl.cnf -key $loc/keys/$rheader\0.pem -out $loc/csr.csr

sleep 5 # Wait slightly to not anger any theoretical rate limits for Let's Encrypt
sudo -u certify python $loc/acme_tiny.py --account-key $account --csr $loc/csr.csr --acme-dir $challenge > $loc/certs/$rcert\0.crt

rm $loc/csr.csr

systemctl reload $service

