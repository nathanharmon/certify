# Certify

Manage elliptic & RSA TLS/SSL keys for web servers using Let's Encrypt.

## Why Use Certify instead of the EFF's Certbot

1. Support for use of both RSA and elliptical key by default
2. Does not require snap (an entire package manager) to install
3. Ability to audit the code (100 lines from certify | 200 from acme_tiny.py)

*For comparison: the certbot 'util.py' file is twice the lines of code of both certify and acme_tiny.py.*

## Installation

Commands starting with # should be run as the root user.

#### Create 'certify' user

Creating a separate user will prevent acme_tiny.py from being able to access the domain private keys.

`# useradd certify --system --no-create-home`

#### Download certify and acme_tiny.py into /opt/

`# wget https://gitlab.com/nathanharmon/certify/-/raw/master/refresh_keys.sh https://raw.githubusercontent.com/diafygi/acme-tiny/master/acme_tiny.py -P /opt/certify/`

Set permissions of the two files.

`# chown certify:certify acme_tiny.py refresh_keys.sh; chmod 500 acme_tiny.py refresh_keys.sh`

#### Create a new private key

`# openssl genrsa 4096 > account.key`

Note that by creating an account key, you are agreeing to the [Let's Encrypt Subscriber Agreement](https://letsencrypt.org/repository/).

**Set the permissions of the account.key file.**

`# chmod 400 account.key`

#### Setup challenge folder and give the certify user access to that folder

`# cd /var/www && mkdir challenges && chown certify:certify challenges`

Add the following to the file in sites-enabled in the server block that listens on port 80.

```
location /.well-known/acme-challenge/ {
    alias /var/www/challenges/;
    try_files $uri =404;
}
```

#### Run refresh_keys.sh

This will auto-generate the remaining files.

`# /opt/certify/refresh_keys.sh`

#### Modify openssl.cnf

- Put your base domain (ex. example.com, example.org) after CN.

- Put your Let's Encrypt registered email after emailAddress.

- Add all of the domain and sub-domains that the certificate should apply to.

#### Run it for real!

`# /opt/certify/refresh_keys.sh`

#### Scheduled Run

Don't forget to add this to the root user's crontab to run once per month.

`0 0 1 * * /opt/certify/refresh_keys.sh`

#### Tell nginx where to look for the certificates and keys

Add the following to the HTTPS block of /etc/nginx/sites-enabled/yourwebsite

```
# Elliptical Keys
ssl_certificate /opt/certify/certs/ellicert0.crt;
ssl_certificate_key /opt/certify/keys/ellippriv0.pem;

# RSA Keys
ssl_certificate /opt/certify/certs/rsacert0.crt;
ssl_certificate_key /opt/certify/keys/rsapriv0.pem;
```

**All done!**

## Extra Security Improvements

These improvements are ordered from most crucial to beneficial. All of these will improve the transport security of your website's users.

#### Redirect non-HTTPS Sites to the Encrypted Version.

Replace your current server block that is listening on port 80 with this, replacing CHANGEME.com to your domain.

```
server {

        location /.well-known/acme-challenge/ {
                alias /var/www/challenges/;
                try_files $uri =404;
        }

        location / {
                return 301 https://$host$request_uri;
        }

        listen 80 ;
        listen [::]:80 ;

        server_name CHANGEME.com;
}
```

#### DNSSEC

This may already be enabled by default on some domain registrars.

- Navigate to your DNS Records (varies by registrar).

- Find DNSSEC and ensure that it is enabled.

#### DNS CAA (prevent other certificate authorities from signing keys for your domain).

- Navigate to your DNS Records (varies by registrar).

- Create a new CAA Record.

| Name | Entry |
| ------ | ------ |
| host | leave it blank |
| flag | 0 |
| tag  | issue |
| value | letsencrypt.org |

- Press save.

This will not work if you get certificates from anywhere other than Let's Encrypt. You will need to include all other certificate authorities in another CAA record.

#### Disable Insecure Transport Protocols (TLS 1.1 and earlier)

TLS 1.1 and earlier are deprecated and should no longer be used because they are insecure.

Add the following to the HTTPS block of /etc/nginx/sites-enabled/yourwebsite

```
ssl_protocols TLSv1.2 TLSv1.3;
```

#### OCSP Stapling

Add the following to the HTTP block of /etc/nginx/nginx.conf

```
# OCSP Stapling
ssl_stapling         on;
ssl_stapling_verify  on;
```

#### Disable Insecure Cipher Suites

ECDSA keys are prefered over RSA keys because of their smaller size, making key exchange faster.

Add the following to the HTTPS block of /etc/nginx/sites-enabled/yourwebsite

```
ssl_ciphers "ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA";
ssl_prefer_server_ciphers on;
```

Or remove all insecure ciphers. This will refuse the connection of some old, insecure clients.

```
ssl_ciphers "ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256";
ssl_prefer_server_ciphers on;
```

#### Diffie-Hellman Parameters

This will increase the RSA key size of the DH key exchange.

Run this command to generate the parameters. This will take a noticably long time.

`# openssl dhparam -out /etc/nginx/dhparam.pem 4096`

Then add the following to the HTTP block of nginx.conf

```
# Diffie-Hellman parameter for DHE ciphersuites
ssl_dhparam          /etc/nginx/dhparam.pem;
```

##### Elliptic Curve Diffie-Hellman

This will remove support for ECDH with a keysize of 256 bits, increasing the size to 521 or 381 bits.

```
# ECDH
ssl_ecdh_curve X25519:secp521r1:secp384r1:X448;
```

#### Strict Transport Security (HSTS)

**Note: HSTS and preloading are not easily reversible. Only enable when you are sure that users will not need access to an insecure HTTP site.**

This will tell returning web browers to only connect via HTTPS. This prevents downgrade attacks from occuring. You must ensure that your website will not need to be accessible by HTTP.

Add the following to the HTTPS block of /etc/nginx/sites-enabled/yourwebsite

```
# HSTS
add_header Strict-Transport-Security "max-age=31536000" always;
```

If you want to apply this rule to subdomains also.

```
# HSTS
add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
```

##### Preloading

To add your domain to the Chrome and Firefox hardcoded list of sites to only access by HTTPS, enable preloading. This will prevent even new users from accessing the HTTP sites, redirecting them to the HTTPS version.

```
# HSTS
add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload" always;
```

Then go to [https://hstspreload.org/](https://hstspreload.org/) and add your domain to the preload submission list.

## Check Your Work

Check your work by going to [https://www.ssllabs.com/ssltest/](https://www.ssllabs.com/ssltest/) and [https://tls.imirhil.fr/](https://tls.imirhil.fr/).

## Troubleshooting

**[SSL Labs](https://ssllabs.com) shows that I have the elliptical key available, but none of my clients are using it.**

Change your [preferred cipher suite order](https://gitlab.com/nathanharmon/certify/-/blob/master/README.md#disable-insecure-cipher-suites) to prioritize elliptical keys over RSA keys.

**I followed the [Extra Security Improvements](https://gitlab.com/nathanharmon/certify/-/blob/master/README.md#extra-security-improvements), but the changes are not reflected in use.**

Check the nginx configuration by running `# nginx -t`

Then restart nginx. `# systemctl reload nginx`

Recheck. If there is still no change, ensure there is no other configuration option that is undoing the change. Start in nginx.conf and look in all of the files that are included.

**There is something wrong with the script.**

Please open a [new issue](https://gitlab.com/nathanharmon/certify/-/issues) describing what is not working properly.
